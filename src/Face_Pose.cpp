#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
// opencv includes
#include <opencv2/opencv.hpp>
// Message filter includes
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_transport/subscriber_filter.h>
// dlib includes
#include <dlib/opencv.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <limits>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>
typedef image_transport::SubscriberFilter imgSuber;
typedef message_filters::sync_policies::ApproximateTime
             <sensor_msgs::Image,sensor_msgs::PointCloud2> MySyncPolicy_;
typedef dlib::rgb_pixel img_type;

typedef pcl::PointXYZRGB PtXYZRGB;
typedef pcl::PointCloud<PtXYZRGB> PCXYZRGB;


//#define data_path '/home/hunter/robotic_adaptive_screen_ws/src/pose_estimation/src/'
struct facePoseEstimator
{
private:
  // ros stuff
  ros::NodeHandle n_;
  image_transport::ImageTransport it_;
  imgSuber RGB_sub_;
  message_filters::Subscriber<sensor_msgs::PointCloud2 > pcl_sub_;
  message_filters::Synchronizer<MySyncPolicy_> sync_;
  ros::Publisher pose_pub_;

  // camera stuff
  sensor_msgs::CameraInfo cam_info;

  // dlib stuff
  dlib::frontal_face_detector face_detector;
  dlib::shape_predictor pose_model;
  dlib::image_window win;

  // global data
  cv::Mat rgb_cv_, cv_dlib_;
  dlib::full_object_detection face_;
  int sample_num_;
  PCXYZRGB::Ptr model_,cloud_;
  // decision stuff
  bool got_data_, have_model_;
  tf::TransformBroadcaster br_;
  
public:
  facePoseEstimator(ros::NodeHandle nh):
    n_(nh),it_(nh),
    RGB_sub_(it_,"/rgb_image",1),
    pcl_sub_(n_,"/cloud",1),
    sync_(MySyncPolicy_(10),RGB_sub_,pcl_sub_),
    face_detector(dlib::get_frontal_face_detector()),
    model_(new PCXYZRGB),cloud_(new PCXYZRGB)
  {
    // initialize pose_model
    dlib::deserialize("/home/hunter/ws_ras/src/pose_estimation/src/shape_predictor_68_face_landmarks.dat") >> pose_model;
    // regester the sync
    sync_.registerCallback(boost::bind(&facePoseEstimator::RGBDCallback,this,_1,_2));

    got_data_ = false;
    have_model_ = false;
    pose_pub_ = n_.advertise<geometry_msgs::PoseStamped>("face_pose",10);
  }

  void RGBDCallback(const sensor_msgs::ImageConstPtr& RGB_img,
                    const sensor_msgs::PointCloud2ConstPtr& msg_cloud)
  {
    sensor_msgs::PointCloud2 tmp_cloud = *msg_cloud;
    pcl::moveFromROSMsg(tmp_cloud,*cloud_);
    // convert to cv image
    rgb_cv_ = cv_bridge::toCvShare(RGB_img,"bgr8")->image;
    cv_dlib_ = cv_bridge::toCvShare(RGB_img, "rgb8")->image;
    // convert RGB to dlib type
    dlib::cv_image<img_type> RGB_dlib(cv_dlib_);
    // find faces
    std::vector<dlib::rectangle> faces = face_detector(RGB_dlib);
    // find face features
    
    /*
      shapes_.clear();
    for(unsigned long i=0;i<faces.size();++i)
    {
      shapes_.push_back(pose_model(RGB_dlib, faces[i]));
    }
    */
    if(faces.size() > 0)
    {
      face_ = pose_model(RGB_dlib,faces[0]);
      got_data_ = true;
    }
    
  }

  bool isNaN(PtXYZRGB pt)
  {
    return !std::isfinite(pt.x) || !std::isfinite(pt.y) || !std::isfinite(pt.z) || pt.x > .1 || pt.y > .1 || pt.z > 1.0;
  }

  void estimatePose()
  {
    /* runs the cv functions for perspective maximization either:
       cv::solvePnP, some ransac verions we will see */
      //std::cout << shapes[0].part(30)(0) << std::endl;
      //win.clear_overlay();
      //win.set_image(RGB_dlib);
      //win.add_overlay(faces,dlib::bgr_pixel(0,0,255)); // visaulize face detection
      //win.add_overlay(dlib::render_face_detections(shapes));

    // show the found nose
    cv::Point2d nose(face_.part(30)(0),face_.part(30)(1));
    cv::circle(rgb_cv_,nose, 3, cv::Scalar(0,0,255), -1);
    cv::imshow("Display",rgb_cv_);
    
    std::cout << "Display Point" << std::endl;
    PCXYZRGB::Ptr new_model_cloud(new PCXYZRGB);
    new_model_cloud->height = 1;
    int nose_idx=-1;
    int u,v;
    // get the points 
    for(int i=17; i<face_.num_parts(); ++i)
    {
      u = face_.part(i)(0);
      v = face_.part(i)(1);
      PtXYZRGB pt = cloud_->at(u,v);
      if(!isNaN(pt))
      {
        new_model_cloud->points.push_back(pt);
        new_model_cloud->width+=1;
        if(i == 30)
        {
          nose_idx = new_model_cloud->width - 1;
        }
      }
    } 

    std::cout << "Made new model" << std::endl;
    std::cout << "Number of good points:" << new_model_cloud->width << std::endl;
    if( (new_model_cloud->width > .5 * face_.num_parts()-17 && nose_idx > 0) )
      // check that we have at least half of the points are good
    {
      /*
      Eigen::Matrix4d inital_guess;
      PtXYZRGB npt = new_model_cloud->points[nose_idx];
      initial_guess << 0.0,0.0,0.0,npt.x, 
        0.0, 0.0, 0.0, npt.y, 
        0.0, 0.0, 0.0, npt.z,
        0.0, 0.0, 0.0, 1.0; */

      pcl::IterativeClosestPoint<PtXYZRGB, PtXYZRGB> icp;
      icp.setInputSource(model_);
      icp.setInputTarget(new_model_cloud);
      PCXYZRGB::Ptr final(new PCXYZRGB);
      icp.align(*final);
      std::cout << "Ran ICP" << std::endl;
      Eigen::Matrix4f transform = icp.getFinalTransformation();
      Eigen::Matrix3f rot_mat = transform.block<3,3>(0,0);
      Eigen::Quaternion<double> q(rot_mat.cast<double>());
      Eigen::Vector3d trans;// = transform.block<3,1>(3,0);
      std::cout << "tf:\n" << transform << "\nrot_mat:\n" << rot_mat << "\ntrans\n" << trans << std::endl;
      //tf::Transform tf;
      //tf.setOrigin(tf::Vector3(trans(0),trans(1),trans(2)));
      //tf.setRotation( tf::Quaternion(q.x, q.y, q.z, q.w) );
      //br_.sendTransform(tf::StampedTransform(tf, ros::Time::now(), "camera_rgb_optical_frame", "face_frame"));
      
    }
    
  }

  void learnModel()
  {
    /* will take ask user to sit still and develop model of the face 
       from 100 images */
 
    std::cout << "Please look at camera and don't move, press enter when ready for model building." << std::endl;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    
    // get first data point
    int num_samples=1;
    int u,v;
    model_->clear();
    model_->resize(face_.num_parts()-17);
    model_->height = 1;
    model_->width  = face_.num_parts()-17;
    bool good = false;

    while(!good)
    {
      ros::spinOnce();
      if(!got_data_)
        continue;
      for(int i=17; i<face_.num_parts(); ++i)
      {
        u = face_.part(i)(0);
        v = face_.part(i)(1);
        if( isNaN(cloud_->at(u,v)) )
        {
          PtXYZRGB pt = cloud_->at(u,v);
          std::cout << "NaN Point " << i 
                << ":[" << pt.x << "," 
                << pt.y << "," 
                << pt.z << "]" << std::endl;
          continue;
        }
      }
      
      good = true;
      for(int i=17; i<face_.num_parts(); ++i)
      {
        u = face_.part(i)(0);
        v = face_.part(i)(1);
        model_->points[i-17] = cloud_->at(u,v);
      } 
    }

    for(int i = 0; i < model_->points.size(); ++i)
    {
      std::cout << "Point " << i 
                << ":[" << model_->points[i].x << "," 
                << model_->points[i].y << "," 
                << model_->points[i].z << "]" << std::endl;
    }
    num_samples = 1;
    while(num_samples < 100)
    {
      ros::spinOnce();
      if(!got_data_)
        continue;
      got_data_ = false;
      
      for(int i=17; i<face_.num_parts(); ++i)
      {
        u = face_.part(i)(0);
        v = face_.part(i)(1);
        if(isNaN(cloud_->at(u,v)))
          continue;
      } 
      
      num_samples++;
      for(int i=17; i<face_.num_parts(); ++i)
      {
        u = face_.part(i)(0);
        v = face_.part(i)(1);
        model_->points[i-17].x += cloud_->at(u,v).x;
        model_->points[i-17].y += cloud_->at(u,v).y;
        model_->points[i-17].z += cloud_->at(u,v).z;
        PtXYZRGB pt = cloud_->at(u,v);
        std::cout << "Point " << i 
                  << ":[" << pt.x << "," 
                  << pt.y << "," 
                  << pt.z << "]" << std::endl;
      } 
    }
    std::cout << "After for loop" << std::endl;
    for(int i = 0; i < model_->points.size(); ++i)
    {
      std::cout << "Point " << i << ":[" << model_->points[i].x << "," << model_->points[i].y << "," << model_->points[i].z << "]" << std::endl;
    }
    for(int i=0; i < model_->width; ++i)
    {
      model_->points[i].x/=100;
      model_->points[i].y/=100;
      model_->points[i].z/=100;
    }
    
    geometry_msgs::Pose init_pose;
    init_pose.position.x = model_->points[30].x;
    init_pose.position.y = model_->points[30].y;
    init_pose.position.z = model_->points[30].z;
    init_pose.orientation.w = 1.0;
    
    for(int i=0; i < model_->width; ++i)
    {
      model_->points[i].x -= model_->points[30].x;
      model_->points[i].y -= model_->points[30].y;
      model_->points[i].z -= model_->points[30].z;
    }

    std::cout << "Model Generated feel free to move" << std::endl;
    /*
    int disp_count = 0;
    while(disp_count < 1000)
    {
      disp_count++;
      geometry_msgs::PoseStamped init_stamped_pose;
      init_stamped_pose.header.stamp = ros::Time::now();
      init_stamped_pose.header.frame_id = std::string("camera_rgb_optical_frame");
      init_stamped_pose.pose = init_pose;
      pose_pub_.publish(init_stamped_pose);
      std::cout << "Published" << std::endl;
      }*/
    for(int i = 0; i < model_->points.size(); ++i)
    {
      std::cout << "Point " << i << ":[" << model_->points[i].x << "," << model_->points[i].y << "," << model_->points[i].z << "]" << std::endl;
    }
    have_model_=true;
    
    
  }

  void spin()
  {
    ros::Rate loop_rate(10);
    while(n_.ok())
    {
      ros::spinOnce();
      if(got_data_)
      {
        got_data_ = false;
        if(have_model_)
        {
          estimatePose();
        }
        else
        {
          learnModel();
        }
      }
      loop_rate.sleep();
    }
  }

};

int main(int argc, char **argv)
{

  std::cout << "Press Enter to Start the Program" << std::endl;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
  
  ros::init(argc,argv,"face_pose_estimator");
  ros::NodeHandle nh;
  facePoseEstimator FPE(nh);
  cv::namedWindow("Display");
  cv::startWindowThread();
  FPE.spin();
  cv::destroyWindow("Display");

  /*
    // Camera internals
    double focal_length = im.cols; // Approximate focal length.
    cv::Point2d center = cv::Point2d(im.cols/2,im.rows/2);
    cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << focal_length, 0, center.x, 0 , focal_length, center.y, 0, 0, 1);
    cv::Mat dist_coeffs = cv::Mat::zeros(4,1,cv::DataType<double>::type); // Assuming no lens distortion
     
    std::cout << "Camera Matrix " << std::endl << camera_matrix << std::endl ;
    // Output rotation and translation
    cv::Mat rotation_vector; // Rotation in axis-angle form
    cv::Mat translation_vector;
     
    // Solve for pose
    cv::solvePnP(model_points, image_points, camera_matrix, dist_coeffs, rotation_vector, translation_vector);
 
     
    // Project a 3D point (0, 0, 1000.0) onto the image plane.
    // We use this to draw a line sticking out of the nose
     
    std::vector<Point3d> nose_end_point3D;
    std::vector<Point2d> nose_end_point2D;
    nose_end_point3D.push_back(Point3d(0,0,1000.0));
     
    cv::projectPoints(nose_end_point3D, rotation_vector, translation_vector, camera_matrix, dist_coeffs, nose_end_point2D);
     
     
    for(int i=0; i < image_points.size(); i++)
    {
      cv::circle(im, image_points[i], 3, cv::Scalar(0,0,255), -1);
    }
     
    cv::line(im,image_points[0], nose_end_point2D[0], cv::Scalar(255,0,0), 2);
     
    std::cout << "Rotation Vector " << std::endl << rotation_vector << std::endl;
    std::cout << "Translation Vector" << std::endl << translation_vector << std::endl;
     
    std::cout <<  nose_end_point2D << std::endl;
     
    // Display image.
    cv::imshow("Output", im);
    cv::waitKey(0);
  */
}
