

#include <dlib/opencv.h>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>

using namespace dlib;
using namespace std;

int main()
{
    try
    {
      cv::VideoCapture cap(0);
        if (!cap.isOpened())
        {
            cerr << "Unable to connect to camera" << endl;
            return 1;
        }
        
        
        cv_image<bgr_pixel> cimg_temp;
        image_window raw_win(cimg_temp,"Raw Image");
        image_window face_win(cimg_temp,"Detected Faces");
        image_window face_pt_win(cimg_temp,"Face Points");
        std::string pose_window_name = "Pose Estimation";
        cv::namedWindow(pose_window_name);
        cv::startWindowThread();

        // 3D model points.
        std::vector<cv::Point3d> model_points;
        model_points.push_back(cv::Point3d(0.0f, 0.0f, 0.0f));         // Nose tip
        model_points.push_back(cv::Point3d(0.0f, -330.0f, -65.0f));    // Chin
        model_points.push_back(cv::Point3d(-225.0f, 170.0f, -135.0f)); // Left eye left corner
        model_points.push_back(cv::Point3d(225.0f, 170.0f, -135.0f));  // Right eye right corner
        model_points.push_back(cv::Point3d(-150.0f, -150.0f, -125.0f));// Left Mouth corner
        model_points.push_back(cv::Point3d(150.0f, -150.0f, -125.0f)); // Right mouth corner

        int model_idx[] = {30,8,36,45,48,54};
        // Load face detection and pose estimation models.
        frontal_face_detector detector = get_frontal_face_detector();
        shape_predictor pose_model;
        deserialize("/home/hunter/ws_ras/src/pose_estimation/src/shape_predictor_68_face_landmarks.dat") >> pose_model;
     
        // Camera internals
        //double focal_length = im.cols; // Approximate focal length.
        //cv::Point2d center = cv::Point2d(im.cols/2,im.rows/2);
        cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << 643.365950, 0.0, 322.247, 
                                 0.0, 650.916345, 266.587841,
                                 0.0, 0.0, 1.0);
        cv::Mat dist_coeffs = (cv::Mat_<double>(4,1) << -0.26514, -.132631, 0.016014, 0.004917);
        //cv::Mat dist_coeffs = cv::Mat::zeros(4,1,cv::DataType<double>::type); // Assuming no lens distortion
        //cout << "Camera Matrix " << endl << camera_matrix << endl ;

        // Grab and process frames until the main window is closed by the user.
        while(!raw_win.is_closed() && !face_win.is_closed() && 
              !face_pt_win.is_closed() && cvGetWindowHandle(pose_window_name.c_str()) )
        {
            // Grab a frame
            cv::Mat im;
            if (!cap.read(im))
            {
                break;
            }

            // Output rotation and translation
            cv::Mat rotation_vector; // Rotation in axis-angle form
            cv::Mat translation_vector;
            // Turn OpenCV's Mat into something dlib can deal with.  Note that this just
            // wraps the Mat object, it doesn't copy anything.  So cimg is only valid as
            // long as temp is valid.  Also don't do anything to temp that would cause it
            // to reallocate the memory which stores the image as that will make cimg
            // contain dangling pointers.  This basically means you shouldn't modify temp
            // while using cimg.
            cv_image<bgr_pixel> cimg(im);

            // Detect faces 
            std::vector<rectangle> faces = detector(cimg);
            // Find the pose of each face.
            std::vector<full_object_detection> shapes;
            for (unsigned long i = 0; i < faces.size(); ++i)
                shapes.push_back(pose_model(cimg, faces[i]));

            std::cout << "Got Shapes and Rectangles" << std::endl;

            if( shapes.size() == 0)
              continue;
            // get the face points from the image
            std::vector<cv::Point2d> image_points;
            for(int i=0; i < model_points.size(); ++i)
            {
              int idx = model_idx[i];
              std::cout << "Idx:" << idx << std::endl;
              int u = shapes[0].part(idx)(0);
              int v = shapes[0].part(idx)(1);
              image_points.push_back(cv::Point2d(u,v));
            }

            std::cout << "Got Image Points" << std::endl;
            // Solve for pose
            cv::solvePnP(model_points, image_points, camera_matrix, 
                         dist_coeffs, rotation_vector, translation_vector);
 
     
            // Project a 3D point (0, 0, 1000.0) onto the image plane.
            // We use this to draw a line sticking out of the nose
            std::cout << "Solve PnP" << std::endl;

            // display the raw image
            raw_win.clear_overlay();
            raw_win.set_image(cimg);

            // display face detection to screen
            face_win.clear_overlay();
            face_win.set_image(cimg);
            face_win.add_overlay(faces, rgb_pixel(255,0,0));

            // Display face points to screen
            face_pt_win.clear_overlay();
            face_pt_win.set_image(cimg);
            face_pt_win.add_overlay(render_face_detections(shapes));

            std::vector<cv::Point3d> nose_end_point3D;
            std::vector<cv::Point2d> nose_end_point2D;
            nose_end_point3D.push_back(cv::Point3d(0,0,1000.0));
     
            projectPoints(nose_end_point3D, rotation_vector, translation_vector, camera_matrix, dist_coeffs, nose_end_point2D);
     
            std::cout << "Project Points" << std::endl;
            for(int i=0; i < image_points.size(); i++)
            {
              cv::circle(im, image_points[i], 3, cv::Scalar(0,0,255), -1);
            }
     
            cv::line(im,image_points[0], nose_end_point2D[0], cv::Scalar(255,0,0), 2);
            
            cout << "Rotation Vector " << endl << rotation_vector << endl;
            cout << "Translation Vector" << endl << translation_vector << endl;
     
            cout <<  nose_end_point2D << endl;
     
            // Display image.
            cv::imshow(pose_window_name, im);
            cv::waitKey(1);
        }
    }
    catch(serialization_error& e)
    {
        cout << "You need dlib's default face landmarking model file to run this example." << endl;
        cout << "You can get it from the following URL: " << endl;
        cout << "   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2" << endl;
        cout << endl << e.what() << endl;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
}


