#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

using namespace std;


int main(int argc, char **argv)
{
  ros::init(argc, argv, "Webcam_Pub");
  cv::VideoCapture cap(4);
  if (!cap.isOpened())
  {
    cerr << "Unable to connect to camera" << endl;
    return 1;
  }
  
  cv::namedWindow("Display");
  cv::startWindowThread();

  ros::NodeHandle n;
  image_transport::ImageTransport it(n);
  image_transport::Publisher image_pub = it.advertise("/webcam", 1);
  
  while( cvGetWindowHandle("Display") )
  {
    cv::Mat image;
    if (!cap.read(image) )
      break;
    cv::imshow("Display",image);
    cv::waitKey(1);

    cv_bridge::CvImage out_msg;
    out_msg.header.stamp = ros::Time::now();
    cv::Mat gray;
    cv::cvtColor(image, gray, CV_BGR2GRAY);
    out_msg.image = gray;
    out_msg.encoding = sensor_msgs::image_encodings::MONO8;
    image_pub.publish(out_msg.toImageMsg());
  }
  cv::destroyWindow("Display");
  return 0;
}
